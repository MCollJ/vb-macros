'Date: 19-Aug-2013
'Author: MC '
'Establishes a connection to a particular database (specified below), pulls
'particular data (price,sf-num,active) and then saves it to a new workbook/csv on
'the desktop (xcart_pricing.csv)
 
'Server     -john.barry.edu.au
'Port       -1234
'Database   -dingo mccoy
'UserId     -flaccid dingo speaker
'Password   -poopshitter
 
Sub GetPriceFromServer()
     
    Dim cnt As ADODB.Connection
    Dim rst As ADODB.Recordset
    Dim stSQL As String
    Dim wbBook As Workbook
    Dim wsSheet As Worksheet
    Dim rnStart As Range
     
     
    Const stADO As String = "DRIVER={MySQL ODBC 5.2 Unicode Driver};" & _
          "SERVER=zzz" & _
          "PORT=1111;" & _
          "DATABASE=n;" & _
          "UID=n;" & _
          "PWD=p;" & _
          "Option=3"
     
    Set wbBook = Workbooks.Add
        With wbBook
            .Title = "xcart_pricing"
        End With
       
    Set wsSheet = wbBook.Worksheets(1)
     
    With wsSheet
        Set rnStart = .Range("A1")
    End With
     
    stSQL = "SELECT xcart_pricing.productid, xcart_pricing.price, xcart_products.forsale " & _
            "FROM xcart_pricing " & _
           "INNER JOIN xcart_products ON xcart_products.productid = xcart_pricing.productid"
 
    Set cnt = New ADODB.Connection
     
    With cnt
        .CursorLocation = adUseClient
        .Open stADO
        .CommandTimeout = 0
        Set rst = .Execute(stSQL)
    End With
     
     'Here we add the Recordset to the sheet from A1
   rnStart.CopyFromRecordset rst
     
     'Cleaning up.
   rst.Close
    cnt.Close
    Application.DisplayAlerts = False
    wbBook.SaveAs FileName:=Environ("USERPROFILE") & "\Desktop\" & "xcart_pricing", FileFormat:=xlCSVWindows
    wbBook.Close SaveChanges:=False
    Application.DisplayAlerts = True
    Set rst = Nothing
    Set cnt = Nothing
     
     
End Sub