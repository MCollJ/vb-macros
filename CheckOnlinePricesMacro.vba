'Date: 19-Jul-2013
'Author: MC '
'This macro compares the prices in a particular worksheet (or the entire workbook) with
'the prices on the site. Which are accessed by a database connection to
'then the database xcart_prices and running the sql command below
 
'SELECT xcart_pricing.productid, xcart_pricing.price, xcart_products.forsale
'FROM xcart_pricing
'INNER JOIN xcart_products ON xcart_products.productid = xcart_pricing.productid
 
'If the price discrepency is greater than 30% then those cells are highlighted green. Cells
'where the discrepency is less than 30% are highlighted in red
'If a product on the masterfile is not found to be in the sql file or it has been disabled
'(i.e it is not on the site) then the cell is highlighted in blue
 
Sub checkOnlinePrices()
 
Dim masterFile As Workbook
Dim productsFile As Workbook
Dim CurrentWorksheet As Worksheet
Dim Cell As Range
Dim Cell2 As Range
Dim rowNumber As Integer
Dim MaxRow As Integer
Dim MaxNumProducts As Integer
Dim fileLocation As String
Dim corruptedString As String
Dim corruptedFlag As Boolean
Dim priceCount As Integer
Dim mfPrice As Single       'Masterfile Price
Dim wsPrice As Single       'Website Price
Dim priceFlag As Boolean    'checks if a large price discrepency has occured
Dim productFlag As Boolean  'check for missing product
 
Dim excludedSheets() As Variant
Dim productID As String
 
Const STARTING_ROW = 10     'beginning row of all products (skips header)
 
priceCount = 0              'tally of incorrectly priced products
priceFlag = False           'tracks whether a product is mis-priced
corruptedFlag = False       'tracks whether a product contains a cell with corrupted data
 
'List of sheets to ignore if the macro is called on them (they contain irrelevant data)
excludedSheets = Array("DupCorsair", "SPARE1", "NOT STOCKED", "vars", "vars2", "ARCHIVE_GDB", "New Products List", "SUNDRY NEW")
 
Set masterFile = ActiveWorkbook
masterFile.Activate
rowNumber = STARTING_ROW
 
'Retrieve the current pricing for all products on the site
Call GetPriceFromServer
 
'Set productsFile = Workbooks.Open("" & fileLocation)
Set productsFile = Workbooks.Open(Environ("USERPROFILE") & "\Desktop\" & "xcart_pricing.csv")
MaxNumProducts = getLastRow()
 
masterFile.Activate
 
On Error GoTo ErrorHandler
 
'Loops through all work sheets in masterfile (To enable, delete "exit sub" at end of loop)
For Each CurrentWorksheet In Worksheets
    rowNumber = STARTING_ROW
    MaxRow = getLastRow()
   
    'Checks if worksheet is one of the ignored ones
   If checkSheet(excludedSheets, CurrentWorksheet.name) = False Then
   
        'Loop through each row in current worksheet
       For Each Cell In Range("b" & rowNumber & ":b" & MaxRow)
        productID = Cell.Text
        productFlag = False
       
        'if productId is not empty
       If productID <> "" Then
       
            'Loop through each row in xcart_pricing from site until product from current row
           'in masterfile is found
           For Each Cell2 In productsFile.Worksheets(1).Range("a1", "a" & MaxNumProducts)
           
                mfPrice = Round(Cell.Cells.Item(1, 11) + 0.0000001, 2) 'The rounding plus .0001
                                                                       'ensures that the price is rounded correctly
               
                'Handles non-numeric data in xcart_pricing cells
               If IsNumeric(Cell2.Cells.Item(1, 2)) = False Then
                Err.Raise description = "file is formatted incorrectly, only numerical values are allowed"
                Exit Sub
                End If
               
                'The pricing for the product in the current row in xcart_pricing
               wsPrice = CSng(Cell2.Cells.Item(1, 2).value)
               
                'Product Found,
               'Row in xcart_pricing contains product price for row in masterfile
               If Cell2 = productID Then
               
                    'Product has been de-activated from site,
                   'colour BLUE
                   If Cell2.Cells.Item(1, 3) = "N" Then
                        Cell.Cells(1, 1).Interior.Color = RGB(135, 206, 250)
                        Exit For
                    End If
                   
                    'Discrepancy in price between price on the site and price in masterfile
                   If Abs(wsPrice - mfPrice) > 1 And wsPrice <> 0 Then
                    count = count + 1
                       
                        'If the difference in price is greater than 30% of the largest price,
                       'Colour GREEN
                       If (Abs(mfPrice - wsPrice) > getMaxSingle(mfPrice, wsPrice) * 0.3) Then
                        priceFlag = True
                        Cell.Cells(1, 1).Interior.Color = RGB(0, 200, 0)
                       
                        'Price difference is less than 30%, colour red
                       Else
                        Cell.Cells(1, 1).Interior.Color = RGB(255, 0, 0)
                       
                        End If
                       
                    'Product is correct price
                   'Colour WHITE
                   Else
                    Cell.Cells(1, 1).Interior.ColorIndex = xlNone
                    End If
                    productFlag = True
                 Exit For
              End If
         Next Cell2
        End If
       
        'All rows in xcart_pricing have been checked, if product was not found then
       'Colour BLUE
       If productFlag = False And Cell.Text <> Empty Then
        Cell.Cells(1, 1).Interior.Color = RGB(135, 206, 250)
        End If
       
       
'Handles cell data that is not numeric. Often cause by copy and pasting with formulas
CorruptedDataErr:
        If corruptedFlag = True Then
            productFlag = False
            For Each Cell2 In productsFile.Worksheets(1).Range("a1", "a" & MaxNumProducts)
                If Cell2 = productID And Cell2.Cells.Item(1, 3) = "Y" Then productFlag = True
            Next Cell2
                If productFlag = False And Cell.Text <> Empty Then
                Cell.Cells(1, 1).Interior.Color = RGB(135, 206, 250) 'Product does not exist on site, colour blue
               End If
        corruptedFlag = False
        End If
        Next Cell
       
        'Display results
       If priceFlag = True Then
        MsgBox ("Number of mispriced products: " & count & _
        vbCrLf & "Red: Online price differs from MasterFile price" & _
        vbCrLf & "Green: Difference in price is greater than 30%" & _
        vbCrLf & "Blue: product is not active on the site")
        Else
        MsgBox ("Number of mispriced products: " & count)
        End If
       
        'List corrupted dataa products for manual checking
       If corruptedFlag = True Then
        MsgBox ("Products possessing corrupted data were:" & corruptedString)
        End If
       
        productsFile.Close
 
    Exit Sub 'Removing this statement will enable checking of the entire masterfile'
   
    End If
Next
 
 
ErrorHandler:
    Select Case Err
   
        'Case 13 is a reference to cells that no longer exist
       Case 13:
        corruptedString = corruptedString & vbCrLf & productID
        corruptedFlag = True
        Resume CorruptedDataErr
       
        Case Else:
        MsgBox ("Error" & Err & ":" & Error(Err))
        productsFile.Close
       
    End Select
 
End Sub