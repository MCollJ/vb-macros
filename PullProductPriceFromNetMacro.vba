Sub ComparisonBCP()
 
Dim HTMLDoc As MSHTML.HTMLDocument
Dim objectCollection As IHTMLElementCollection
Dim IEApp As InternetExplorer
Dim Rng As Range
Dim SelectedArea As Range
Dim ResultArray() As Integer
Dim Products As Collection
Dim Product As ProductClass
 
Set Products = New Collection
 
'Get Products to update------------------------------->
On Error Resume Next
    Set Rng = Application.InputBox(prompt:="Select the prices you want to update", _
            Title:="Competitor Price Update", Type:=8)
 
On Error Resume Next
    Set SelectedArea = Application.InputBox(prompt:="Select the MFR P/N column (DONT use SPN)", _
            Title:="Field to use in search", Type:=8)
 
If SelectedArea Is Nothing Then Exit Sub
   
    col_MPN = SelectedArea.Column
    lastRow = Rng(Rng.Cells.Count).Row
    col_PRICE = Rng.Column
    col_OURPRICE = 12
'<---------------------------------------------------
 
Set IEApp = New InternetExplorer
 
IEApp.Visible = False
 
'MAIN LOOP------------------------------------------->
 
For rowCount = Rng.Row To lastRow
 
mpn = Cells(rowCount, col_MPN).Value
 
If mpn <> Empty Then
 
IEApp.Navigate "http://www.shopbot.com.au/m/?m=" & mpn
 
    While IEApp.Busy
    Wend
    While IEApp.document.readyState <> "complete"
    Wend
   
    Set HTMLDoc = IEApp.document
   
    'Application.Wait Time + TimeSerial(0, 0, 0.5)
    'Dim Timer As Date
    'Timer = TimeValue(Now) + TimeSerial(0, 0, 2)
   
     Do
     'If TimeValue(Now) > Timer Then
   ' Exit Do
    Loop While IEApp.readyState <> 4
   
    'Checks for no product found------------------------>
   Set objectCollection = HTMLDoc.getElementsByTagName("strong")
    For Each elem In objectCollection
        If (Trim(elem.innerText) = "We found no exact matches for your request. Below you will find results containing at least one of your keywords.") Then
        GoTo ContinueFlag
        End If
    Next
   
    Set objectCollection = HTMLDoc.getElementsByTagName("p")
   
    For Each elem In objectCollection
        If (Trim(elem.innerText) = "No Results Found") Then
        GoTo ContinueFlag
        End If
    Next
    '<---------------------------------------------------
   
    'Set objectDescCollection = HTMLDoc.getElementsByClassName("pNameLong")
   'Set objectPriceCollection = HTMLDoc.getElementsByClassName("price")
   'For i = 1 To objectPriceCollection.Length
   'Set Product = New ProductClass
   'Product.desc = objectDescCollection(i).innerText
   'Product.price = objectPriceCollection(i).innerText
   'Products.Add Product
   'Debug.Print i & Product.desc
   'Next
   Set objectPriceCollection = HTMLDoc.getElementsByClassName("price")
    Dim lowest As Double
    lowest = GrabPrice(objectCollection, Cells(rowCount, col_OURPRICE).Value, 0.3)
       
    'Check that shopbot price is not exorbitantly low
   If lowest = 99999999 Then
    resultString = ""
    Else
    resultString = CStr(lowest)
    End If
       
    MyPath = "http://www.shopbot.com.au/m/?m=" & mpn
   
    Cells(rowCount, col_PRICE).Formula = "=HYPERLINK(""" & MyPath & """,""" & resultString & """)"
End If
ContinueFlag:
Next
'<----------------------------------------------------------------------------
 
    IEApp.Quit
   
End Sub