''
' Update prices in XCart database
'
'
Sub UpdatePrice()
   
Dim cnt As ADODB.Connection
Dim Rng As Range
Dim rowCount As Integer
Dim masterFile As Workbook
Dim plRowCount As Integer
Dim myQuery As String
 
Const stADO As String = "DRIVER={MySQL ODBC 5.2 Unicode Driver};" & _
          "SERVER=eatshit.com;" & _
          "PORT=3306;" & _
          "DATABASE=my_ass;" & _
          "UID=abcdfuckefg;" & _
          "PWD=penguinsarewilddogs;" & _
          "Option=3"
         
   
    Set masterFile = ActiveWorkbook
   
    ''' ask user for products (rows) to update
   On Error Resume Next
    Set Rng = Application.InputBox(prompt:="Select the products you want to update", _
            Title:="Price update CSV", Type:=8)
   
    If Rng Is Nothing Then Exit Sub
   
    ''' stop screen flicker!
   Application.ScreenUpdating = False
   
    lastRow = Rng(Rng.Cells.Count).Row  ''' last row of the selected range
   
    ''' Find which columns contain the productid, list price and price (OI) respectively
   ''' in the masterfile
   On Error Resume Next
    productidColumn = ColLetter(Cells.Find(What:="!PRODUCTID", _
                LookAt:=xlWhole, _
                SearchOrder:=xlByRows, _
                SearchDirection:=xlNext, _
                MatchCase:=False).Column)
   
    listPriceColumn = ColLetter(Cells.Find(What:="!LIST_PRICE", _
                LookAt:=xlWhole, _
                SearchOrder:=xlByRows, _
                SearchDirection:=xlNext, _
                MatchCase:=False).Column)
               
    priceColumn = ColLetter(Cells.Find(What:="!PRICE", _
                LookAt:=xlWhole, _
                SearchOrder:=xlByRows, _
                SearchDirection:=xlNext, _
                MatchCase:=False).Column)
   
    If Err > 0 Then
        MsgBox "Couldn't find header row in Master File"
        End
    End If
   
    Set cnt = New ADODB.Connection
   
    With cnt
        .CursorLocation = adUseClient
        .Open stADO
        .CommandTimeout = 0
       
    End With
   
     
      ''' select the relevant data from the blue areas
   For rowCount = Rng.Row To lastRow
 
        Range(productidColumn & rowCount & "," & listPriceColumn & rowCount & "," & _
                priceColumn & rowCount).Select
               
        If Cells(rowCount, listPriceColumn) <> "" And Cells(rowCount, priceColumn) <> "" Then
                myQuery = "UPDATE xcart_pricing " & _
                          "SET price =" & Cells(rowCount, priceColumn) & _
                          " WHERE productid =" & Cells(rowCount, productidColumn)
               
                With cnt
                .Execute (myQuery)
                End With
               
                myQuery = "UPDATE xcart_products" & _
                          " SET list_price =" & Cells(rowCount, listPriceColumn) & _
                          " WHERE productid =" & Cells(rowCount, productidColumn)
               
                With cnt
                .Execute (myQuery)
                End With
               
                If Err > 0 Then
                    Debug.Print Error
        End If
            plRowCount = plRowCount + 1
        End If
    Next rowCount
    cnt.Close
   
    Application.ScreenUpdating = True
    Set cnt = Nothing
 
End Sub